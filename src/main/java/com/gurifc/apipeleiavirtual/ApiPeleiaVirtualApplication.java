package com.gurifc.apipeleiavirtual;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiPeleiaVirtualApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiPeleiaVirtualApplication.class, args);
	}

}
